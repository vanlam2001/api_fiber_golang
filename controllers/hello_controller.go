package controllers

import (
	"github.com/gofiber/fiber/v2"
)

// HelloWorld trả về chuỗi "Hello, World!"
func HelloWorld(c *fiber.Ctx) error {
	return c.SendString("Hello, World!")
}

func Test_String(c *fiber.Ctx) error {
	return c.SendString("Test String")
}
