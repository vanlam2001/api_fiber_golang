package controllers

import (
	"my-api/models"
	"strconv"

	"github.com/gofiber/fiber/v2"
)

var users []models.User

// GetAllUsers gets all users
func GetAllUsers(c *fiber.Ctx) error {
	return c.JSON(users)
}

// GetUserByID gets a user by ID
func GetUserByID(c *fiber.Ctx) error {
	// Parse the user ID from the URL parameter
	id := c.Params("id")

	// Find the user in the slice based on the ID
	for _, user := range users {
		if user.ID == id {
			return c.JSON(user)
		}
	}

	// User not found
	return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
		"message": "User not found",
	})
}

// CreateUser creates a new user
// CreateUser creates a new user
func CreateUser(c *fiber.Ctx) error {
	var newUser models.User

	// Parse the request body into the new user struct
	if err := c.BodyParser(&newUser); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Invalid request body",
		})
	}

	// Assign a unique ID to the new user
	newUser.ID = strconv.Itoa(len(users) + 1)

	// Add the new user to the slice
	users = append(users, newUser)

	return c.Status(fiber.StatusCreated).JSON(newUser)
}

// UpdateUser updates a user by ID
func UpdateUser(c *fiber.Ctx) error {
	// Parse the user ID from the URL parameter
	id := c.Params("id")

	// Parse the request body into an updated user struct
	var updatedUser models.User
	if err := c.BodyParser(&updatedUser); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Invalid request body",
		})
	}

	// Find the user in the slice based on the ID
	for i, user := range users {
		if user.ID == id {
			// Update the user in the slice
			users[i] = updatedUser
			return c.Status(fiber.StatusOK).JSON(updatedUser)
		}
	}

	// User not found
	return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
		"message": "User not found",
	})
}

// DeleteUser deletes a user by ID
func DeleteUser(c *fiber.Ctx) error {
	// Parse the user ID from the URL parameter
	id := c.Params("id")

	// Find the user in the slice based on the ID
	for i, user := range users {
		if user.ID == id {
			// Remove the user from the slice
			users = append(users[:i], users[i+1:]...)
			return c.Status(fiber.StatusNoContent).SendString("")
		}
	}

	// User not found
	return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
		"message": "User not found",
	})
}
