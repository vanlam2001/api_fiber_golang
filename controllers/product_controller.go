package controllers

import (
	"my-api/models"

	"github.com/gofiber/fiber/v2"
)

var products []models.Product

// GetAllProducts returns all products

func GetAllProducts(c *fiber.Ctx) error {
	return c.JSON(products)
}

// CreateProduct creates a new product

func CreateProduct(c *fiber.Ctx) error {
	var newProduct models.Product

	// Parse yêu cầu từ người dùng vào biến newProduct
	if err := c.BodyParser(&newProduct); err != nil {
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{
			"message": "Invalid request body",
		})
	}

	// Gán ID duy nhất cho sản phẩn mới
	newProduct.ID = uint(len(products) + 1)

	// Thêm sản phẩm vào danh sách
	products = append(products, newProduct)

	return c.Status(fiber.StatusCreated).JSON(newProduct)
}
