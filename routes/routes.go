package routes

import "github.com/gofiber/fiber/v2"

func SetupRoutes(app *fiber.App) {
	// Define your routes here
	UserRoutes(app)
	// Add more routes as needed
	HelloRoutes(app)
	Test(app)
	OkRoutes(app)
	ProductRoutes(app)
}
