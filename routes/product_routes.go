package routes

import (
	"my-api/controllers"

	"github.com/gofiber/fiber/v2"
)

func ProductRoutes(app *fiber.App) {
	product := app.Group("/api/product")

	product.Get("/", controllers.GetAllProducts)
	product.Post("/", controllers.CreateProduct)
}
