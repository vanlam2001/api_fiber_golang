package routes

import (
	"my-api/controllers"

	"github.com/gofiber/fiber/v2"
)

func HelloRoutes(app *fiber.App) {
	hello := app.Group("/api/hello")

	hello.Get("/", controllers.HelloWorld)
	hello.Get("/test-string", controllers.Test_String)
}
