package routes

import (
	"my-api/controllers"

	"github.com/gofiber/fiber/v2"
)

func OkRoutes(app *fiber.App) {
	ok := app.Group("/api/ok")

	ok.Get("/", controllers.Ok)
}
