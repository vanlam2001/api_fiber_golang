package routes

import (
	"my-api/controllers"

	"github.com/gofiber/fiber/v2"
)

func UserRoutes(app *fiber.App) {
	user := app.Group("/api/user")

	user.Get("/", controllers.GetAllUsers)
	user.Get("/:id", controllers.GetUserByID)
	user.Post("/", controllers.CreateUser)
	user.Put("/:id", controllers.UpdateUser)
	user.Delete("/:id", controllers.DeleteUser)
}
