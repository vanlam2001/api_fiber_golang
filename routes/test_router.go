package routes

import (
	"my-api/controllers"

	"github.com/gofiber/fiber/v2"
)

func Test(app *fiber.App) {
	test := app.Group("/api/test")

	test.Get("/", controllers.Test)
}
