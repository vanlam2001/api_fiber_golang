package main

import (
	"log"
	"my-api/config"
	"my-api/routes"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
)

func main() {
	// Load environment variables from .env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// Create a new Fiber app
	app := fiber.New()

	// Configure your app
	config.Configure(app)

	// Define routes
	routes.SetupRoutes(app)

	// Start the server
	err = app.Listen(":4000")
	if err != nil {
		log.Fatal(err)
	}
}
