package config

import (
	"github.com/gofiber/fiber/v2"
)

// Configure configures the Fiber app
func Configure(app *fiber.App) {
	// Add middleware, CORS, database configuration, etc. here
}
